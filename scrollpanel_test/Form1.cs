﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace scrollpanel_test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        Point NewDragLocation = new Point(0, 0);
        Point NewDragVelocity = new Point(0, 0);

        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                NewDragLocation = e.Location;
                panel2.BackColor = Color.Blue;
                this.Text = NewDragLocation.ToString();
            }

        }

        private void panel2_MouseUp(object sender, MouseEventArgs e)
        {
            panel2.BackColor = Color.Green;


            panel2.Location = new Point(panel2.Location.X + (e.X - NewDragLocation.X), panel2.Location.Y + (e.Y - NewDragLocation.Y));
            this.Text = "UP: " + panel2.Location;
        }


    }
}
