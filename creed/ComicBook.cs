﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;

namespace creed.book
{
    ///<summary>
    ///
    ///Comicbook class
    ///
    ///ComicBooks contain pages. There are three ways to actually load a comic book.
    ///
    ///Load(string source_directory):
    ///	Loads a directory as a Comicbook.
    ///
    ///	LoadArchive(string source_archive):
    ///		Loads an archive (using 7z).
    ///		This will load *streams* that get used once then disposed of properly. 
    ///
    ///</summary>
    public class ComicBook
    {

        // Comic books contain Pages.
        List<ComicBookPage> pages = new List<ComicBookPage>();
        int current_page;

        public delegate void PageChangedEventHandler(object sender, int newPage);
        public delegate void LoadedPage(object sender, string filename, int num, int max);
        public delegate void LoadedBook(object sender, EventArgs e);

        public event PageChangedEventHandler PageChanged;
        public event LoadedPage PageLoaded;
        public event LoadedBook BookLoaded;
        public bool Loaded { get { return _isLoaded; } }
        bool _isLoaded = false;

        Regex isImageRegex = new Regex("(jpg|png|tiff|bmp)");

        public ComicBook()
        { }
        public void Load(string SourceDir)
        {

            pages = new List<ComicBookPage>(); // clear the pages.
            // Load the pages from a directory.
            DirectoryInfo inf = new System.IO.DirectoryInfo(SourceDir);
            FileInfo[] files = inf.GetFiles();
            int cLoadedPage = 0;
            foreach (var file in files.Where((FileInfo a) => { return isImageRegex.IsMatch(a.Extension); }))
            {
                // Load each file 
                ComicBookPage pg = new ComicBookPage(file.FullName, file.Name);
                pg.PageNum = cLoadedPage;
                pg.Comment = file.Name;
                pages.Add(pg);
                //if (PageLoaded != null)
                //{
                PageLoaded(this, file.Name, ++cLoadedPage, pages.Count);
                //}
            }
            _isLoaded = true;
            current_page = 0;

            if (this.BookLoaded != null)
            {
                BookLoaded(this, EventArgs.Empty);
            }

            //this.PageChanged(this, 0);


        }
		// Loads an Archive from 7zip.dll
        public void LoadArchive(string ArchiveSource)
        {
            pages = new List<ComicBookPage>();
            // Load from an archive.
            SevenZip.SevenZipExtractor ex = new SevenZip.SevenZipExtractor(ArchiveSource);
            var images = ex.ArchiveFileData.Where((SevenZip.ArchiveFileInfo inf) =>
            {
                int dotPos = inf.FileName.LastIndexOf('.');
                if (dotPos > 0 && !inf.IsDirectory)
                {
                    return ( isImageRegex.IsMatch( inf.FileName.Substring(dotPos ) )  ) && !inf.IsDirectory;
                }
                else
                { return false; }
            }
                    ); // where
            int cLoadedPage=0;
            foreach (var image in images ) 
            {
                Stream r = new MemoryStream();
                ex.ExtractFile(image.Index, r);

                ComicBookPage pg = new ComicBookPage(r, image.FileName);
                PageLoaded(this, image.FileName, ++cLoadedPage, images.Count());
                pg.PageNum = cLoadedPage;
                pg.Comment = image.FileName;
                pages.Add(pg);
            }
            _isLoaded = true;
            current_page = 0;

            if (this.BookLoaded != null)
            {
                BookLoaded(this, EventArgs.Empty);
            }
        }

        public IEnumerator<ComicBookPage> GetEnumerator()
        {
            return pages.GetEnumerator();
        }

        public void NextPage()
        {
            if (current_page + 1 < pages.Count)
            {
                current_page += 1;
                CallPageChanged();
            }
        }
        public void PrevPage()
        {
            if (current_page > 0)
            {
                current_page -= 1;
                CallPageChanged();
            }
        }

        public void gotoPage(int page)
        {
            if (page >= 0 && page < pages.Count)
            {
                current_page = page;
                CallPageChanged();
            }

        }

        private void CallPageChanged()
        {
            if (PageChanged != null)
            {
                PageChanged(this, current_page);
            }
        }

        public int LastPage { get { return pages.Count - 1; } }

        public ComicBookPage this[int pagenum]
        {
            get
            {
                if (pagenum >= 0 && pagenum < pages.Count)
                    return pages[pagenum];
                else
                    return new ComicBookPage();
            }
        }

    }



    /// <summary>
    /// ComicBookPages contain a page and 
    /// </summary>
    public class ComicBookPage
    {
        public int PageNum { get; set; }
        Image _tImage = null;
        private Stream _imstream;
        
        public Image Page
        {
            get
            {
                // if we haven't gotten the image loaded yet, set it up and clean up.
                if (_tImage == null)
                {
                    _tImage = Image.FromStream(_imstream);
                    _imstream.Close();
                    _imstream.Dispose();
                }
                // Since we should have an image by now, we'll just go with it.
                return _tImage;
            }
        }
        public String Comment { get; set; }

        public ComicBookPage() { }
        public ComicBookPage(string Filename)
        {
            _imstream = System.IO.File.OpenRead(Filename);
            Comment = "";
        }
        public ComicBookPage(string Filename, string comment)
        {
            _imstream = System.IO.File.OpenRead(Filename);
            Comment = comment;
        }

        public ComicBookPage(Image img)
        {
            _tImage = img;

        }

        public ComicBookPage(Image img, string Comment)
        {
            _tImage = img;
            this.Comment = Comment;
        }

        public ComicBookPage(Stream stream, string Comment)
        {
            _imstream = stream;
        }

    }



}
