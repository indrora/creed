﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace creed
{
    public partial class pagelist : Form
    {
        public pagelist()
        {
            InitializeComponent();

        }



        public delegate void SelectedPageEvent(int pageNum);

        public event SelectedPageEvent Page_Selected;

        private int _tPageNum;

        public void getBook(book.ComicBook b)
        {
            listView1.Items.Clear();
            foreach (var page in b)
            {
                ListViewItem li = new ListViewItem();
                li.Text = ""+page.PageNum;
                li.SubItems.Add(page.Comment);  
                    listView1.Items.Add(li);
            }
        }

        private void do_pageselect()
        {
        	if (Page_Selected != null && _tPageNum.Equals(null) == false )
            {
                Page_Selected(_tPageNum);
            }
        }

        private void listView1_ItemActivate(object sender, EventArgs e)
        {
            _tPageNum = listView1.SelectedIndices[0];
            do_pageselect();
        }

        private void pagelist_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.Hide();
            }
        }
    }
}
