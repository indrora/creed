﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace creed
{
    [Serializable]
    class CreedSettings
    {

        [DefaultValue(false)]
        public bool useMiddleMouseButton { get; set; }

        [DefaultValue(true)]
        public bool DoubleClickGoesForward { get; set; }

        static CreedSettings mySettings = null;

        public CreedSettings()
        {
            string WhereAmI = (new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().FullName)).DirectoryName;
            if (System.IO.File.Exists(WhereAmI + @"\settings.xml"))
            {
                mySettings = (CreedSettings)( // Begin cast to CreedSettings
                    new System.Xml.Serialization.XmlSerializer( // Create XML Serializer
                        typeof(CreedSettings) // of type CreedSettings
                        ).Deserialize( // With which to Deserialize
                            System.IO.File.OpenRead( // A file stream
                                WhereAmI + @"\settings.xml" // shoved on disk.
                            )
                        )
                    );
            }
            else
            {
                mySettings = this;
            }
        }

        public static CreedSettings c_settings
        {
            get { if (mySettings == null) { mySettings = new CreedSettings(); return mySettings; } else return mySettings; }
            set { mySettings = value; }
        }


    }
}
