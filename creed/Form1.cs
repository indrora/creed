﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace creed
{
    public partial class Form1 : Form
    {

        Point MouseDragStart = new Point();

        MouseButtons dragbutton { get { return MouseButtons.Left; } }


        bool _mangamode;
        bool MangaMode
        {
            get { return _mangamode; }
            set
            {
                _mangamode = value;
                mnu_mangamode.Checked = value;
                toolbar_mangamode.Checked = value;
            }
        }

        bool _alwaysfitmode;
        bool AlwaysFitMode
        {
            get { return _alwaysfitmode; }
            set
            {
                _alwaysfitmode = value;
                mnu_alwaysfit.Checked = value;
                toolbar_alwaysfit.Checked = value;
                AlignPicture();
            }
        }

        bool LockCursor = false;
        Point cursorLockPos = new Point(0, 0);

        FolderBrowserDialog open_folder_as_archive_dlg = new FolderBrowserDialog();
        OpenFileDialog open_archive_dialog = new OpenFileDialog();

        pagelist pgList = new pagelist();

        book.ComicBook book = new book.ComicBook();
        public Form1()
        {
            init();
        }

        public void init()
        {
            InitializeComponent();
            book.PageChanged += new creed.book.ComicBook.PageChangedEventHandler(book_PageChanged);
            book.PageLoaded += new creed.book.ComicBook.LoadedPage(book_PageLoaded);
            book.BookLoaded += new creed.book.ComicBook.LoadedBook(book_BookLoaded);
            pgList.Page_Selected += new pagelist.SelectedPageEvent(pgList_Page_Selected);
            pgList.Owner = this.FindForm();

            this.MouseWheel += new MouseEventHandler(Form1_MouseWheel);
            pictureBox1.MouseWheel += new MouseEventHandler(Form1_MouseWheel);

            pictureBox1.Visible = false;
        }

        void Form1_MouseWheel(object sender, MouseEventArgs e)
        {
            // We're going to attempt to move the picture left/right and side/side.
            Console.WriteLine("Old position: {0} ", pictureBox1.Location);
            Point newLocation = new Point(pictureBox1.Location.X, pictureBox1.Location.Y + e.Delta);
            Rectangle cImageRect = new Rectangle(newLocation, pictureBox1.Size);
            Rectangle cPanelRect = new Rectangle(new Point(0, 0), panel1.ClientSize);

            pictureBox1.Location = GetClippedPoint(
               newLocation,
                cImageRect,
                cPanelRect);

            Console.WriteLine("New Position: {0}", pictureBox1.Location);

        }

        public Form1(string[] args)
        {
            if (args.Length > 0)
            {
                try
                {
                    var ArchiveType = new SevenZip.SevenZipExtractor(args[0]);
                }
                catch
                {
                    // Les Fail. 
                    MessageBox.Show("You have supplied an invalid archive!");
                    this.Close();
                    this.Dispose();
                    return;

                }

            }

            init();
        }

        void pgList_Page_Selected(int pageNum)
        {
            book.gotoPage(pageNum);
        }

        void book_BookLoaded(object sender, EventArgs e)
        {
            pgList.getBook(book);
            book.gotoPage(0);
        }

        void book_PageLoaded(object sender, string filename, int num, int max)
        {
            this.current_status.Text = filename;
            this.current_progress.Minimum = 0;
            if (current_progress.Maximum != max) { current_progress.Maximum = max; }
            current_progress.Value = num;
            current_status.Invalidate();

        }

        void book_PageChanged(object sender, int newPage)
        {
            pictureBox1.Visible = true;
            pictureBox1.Image = book[newPage].Page;

            int horiz_offset = (int)(0.5 * panel1.ClientSize.Width);
            horiz_offset -= (int)(pictureBox1.Image.Width / 2f);
            pictureBox1.Location = new Point((MangaMode ? -1 * pictureBox1.Image.Width : 0), 0);
            //pictureBox1.Size = pictureBox1.Image.Size;

            imgPosLabel.Text = " img:" + pictureBox1.Location.ToString();


            current_status.Text = String.Format("( Page {0} of {1} )", book[newPage].PageNum, book.LastPage);
            AlignPicture();
        }

        private void openFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            open_folder_as_archive_dlg.Description = "Locate the directory the comic book is located in";
            if (open_folder_as_archive_dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                book.Load(open_folder_as_archive_dlg.SelectedPath);
            }
        }


        #region page movement
        private void next_page()
        {
            if (book.Loaded)
            {
                book.NextPage();
            }
        }
        private void prev_page()
        {
            if (book.Loaded)
            {
                book.PrevPage();
            }
        }

        private void first_page()
        {
            if (book.Loaded)
            {
                book.gotoPage(0);
            }

        }
        private void last_page()
        {
            if (book.Loaded)
            {
                book.gotoPage(book.LastPage);
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            prev_page();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            next_page();
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            next_page();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            first_page();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            last_page();
        }

        #endregion

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == dragbutton)
            {
                /*   if (!LockCursor)
                   {
                       LockCursor = true;
                       cursorLockPos = e.Location;

                   }*/

                MouseDragStart = e.Location;


            }
            //LockCursor = false;

        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == dragbutton)
            {

                Point newLocation = new Point(pictureBox1.Location.X + (e.X - MouseDragStart.X), pictureBox1.Location.Y + (e.Y - MouseDragStart.Y));
                // Check if our rectangle isn't going to break our long haul.
                // the image shouldn't be able to leave the rectangle entirely
                Rectangle cImageRect = new Rectangle(newLocation, pictureBox1.Size);
                Rectangle cPanelRect = new Rectangle(new Point(0, 0), panel1.ClientSize);

                pictureBox1.Location = GetClippedPoint(newLocation, cImageRect, cPanelRect);

                if (LockCursor)
                {
                    
                    Cursor.Position = cursorLockPos;
                }



            }
            if (e.Delta != 0)
            {
                Point newLocation = new Point(pictureBox1.Location.X, pictureBox1.Location.Y + (10 * e.Delta));
                Rectangle cImageRect = new Rectangle(newLocation, pictureBox1.Size);
                Rectangle cPanelRect = new Rectangle(new Point(0, 0), panel1.ClientSize);

                pictureBox1.Location = GetClippedPoint(newLocation, cImageRect, cPanelRect);
            }
        }

        private void pictureBox1_LocationChanged(object sender, EventArgs e)
        {

            imgPosLabel.Text = " img:" + pictureBox1.Location.ToString();

        }

        private Point GetClippedPoint(Point target, Rectangle imageRect, Rectangle ViewportRect)
        {

            Point endPoint = target;

            // We'll just center it.

            if (ViewportRect.Width > imageRect.Width)
            {

                endPoint.X = (ViewportRect.Width / 2) - (imageRect.Width / 2);
            }
            else if (ViewportRect.Left <= imageRect.Left)
            {
                // the left hand side of the viewport is at this point to the LEFT of the image.
                // this means we have hit the LEFT side of the image, and should clip our X to 0.
                endPoint.X = 0;
            }
            else if (ViewportRect.Right >= imageRect.Right)
            {
                // the right hand side of the viewport is now past the right hand side of the image.
                // this means we have hit the right hand side and should clip our X to (imageRect.Width-ViewportRect.Width)
                endPoint.X = ViewportRect.Width - imageRect.Width; ;

            }

            if (ViewportRect.Height > imageRect.Height)
            {
                endPoint.Y = (ViewportRect.Height / 2) - (imageRect.Height / 2);

            }
            else if (ViewportRect.Top <= imageRect.Top)
            {
                // the top of the viewport has been hit.
                // we clip our Y here to 0.
                endPoint.Y = 0;
            }
            else if (ViewportRect.Bottom >= imageRect.Bottom)
            {
                // We've hit the bottom of the image.
                // Here we need to clip to ( ImageRect.Height - ViewportRect.Height )
                endPoint.Y = (ViewportRect.Height - imageRect.Height);
            }

            return endPoint;

        }


        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void panel1_Resize(object sender, EventArgs e)
        {
            AlignPicture();
        }

        private void AlignPicture()
        {
            if (!_alwaysfitmode)
            {
                if (pictureBox1.SizeMode != PictureBoxSizeMode.AutoSize) pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;
                pictureBox1.Location = GetClippedPoint(pictureBox1.Location, new Rectangle(pictureBox1.Location, pictureBox1.Size), panel1.ClientRectangle);
                pictureBox1.Dock = DockStyle.None;
                
            }
            else
            {
                if (pictureBox1.SizeMode != PictureBoxSizeMode.Zoom) pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
                pictureBox1.Location = new Point(0, 0);
                pictureBox1.Dock = DockStyle.Fill;

            }
        }

        private void mnu_mangamode_Click(object sender, EventArgs e)
        {
            MangaMode = !MangaMode;
        }

        private void toolbar_mangamode_Click(object sender, EventArgs e)
        {
            MangaMode = !MangaMode;
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            open_archive_dialog.Filter = "Comic book archive (*.cbr, *.cbz, *.cb7z)|*.cbz;*.cbr;*.cb7z|All Files (*.*)|*.*";
            if (open_archive_dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                book.LoadArchive(open_archive_dialog.FileName);
            }
        }
        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //ShellAbout(this.Handle, "Creed", "Creed is a comic book reader for Windows", (this.Icon.ToBitmap().GetHicon()));
            // 
        }


        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            if (pgList.Visible == false)
            {
                pgList.Show();
            }
            else
            {
                pgList.BringToFront();
            }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            AlignPicture();
        }

        private void mnu_alwaysfit_Click(object sender, EventArgs e)
        {
            AlwaysFitMode = !AlwaysFitMode;
        }



    }

}
